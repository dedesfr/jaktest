@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" method="GET" action="{{ route('order.list') }}">
                <div class="form-group{{ $errors->has('order_number') ? ' has-error' : '' }}">
                    <label for="order_number" class="col-md-4 control-label">Search Order Number</label>

                    <div class="col-md-6">
                        <input id="order_number" type="number" class="form-control" name="order_number">
                    </div>
                </div>
            </form>
            <div class="panel panel-default">
                <div class="panel-heading">Order List</div>
                <div class="panel-body">
                    @if ( !empty ($orders->first()))
                        <table class="table table-striped" id="users">
                            <thead>
                            <tr>
                                <th>Order No</th>
                                <th>Description</th>
                                <th>Total</th>
                                <th>Information</th>
                            </tr>					
                            </thead>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>{{ $order->order_number }}</td>
                                        @if ($order->phone != "")
                                            <td>{{$order->total}} for {{$order->phone}}</td>
                                        @else
                                            <td>{{$order->product}} for {{$order->total}}</td>
                                        @endif
                                        <td>{{ $order->grand_total }}</td>
                                        @if ($order->status == "pending")
                                            <td><a href="{{route('order.pay', $order->order_number)}}" class="btn btn-default">Pay Here</a></td>
                                        @elseif ($order->status == "success")
                                            @if ($order->product != "")
                                                <td>Shipping Code : {{$order->shipping_code}}</td>
                                            @else
                                                <td>Success</td>
                                            @endif
                                        @else
                                            <td>fail</td>
                                        @endif
                                    </tr>
                                @endforeach
                            </table>
                        @else
                            <h2>Belum ada order</h2>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
