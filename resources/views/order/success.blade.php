@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Order Success</div>

                <div class="panel-body">
                    @if ($phone != "")
                        <h4>Your Order Number</h4>
                        <h4>{{$order_id}}</h4>
                        <br>
                        <h4>Total</h4>
                        <h4>{{$total + $grandTotal5}}</h4>
                        <br>
                        <h4>Your Mobile Phone Number {{$phone}} will be topped up for {{$total}} after you pay</h4>

                        <div class="col-md-6 col-md-offset-4">
                            <a class="btn btn-primary" href="{{route('order.pay', $order_id)}}">Pay Here</a>
                        </div>
                    @else
                        <h4>Your Order Number</h4>
                        <h4>{{$order_id}}</h4>
                        <br>
                        <h4>Total</h4>
                        <h4>{{$total + 10000}}</h4>
                        <br>
                        <h4>{{$product}} that cost {{$total + 10000}} will be shipped to {{$address}} after you pay</h4>
                        <div class="col-md-6 col-md-offset-4">
                            <a class="btn btn-primary" href="{{route('order.pay', $order_id)}}">Pay Here</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
