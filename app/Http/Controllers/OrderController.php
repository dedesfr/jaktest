<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order as Order;
use Auth;

class OrderController extends Controller
{
    public function index(Request $request){
        if($request->order_number != "") {
            $user_id = Auth::user()->id;
            $orders = Order::where('order_number', 'like' , '%' .$request->order_number. '%')->where('user_id', $user_id)->get();
        } else {
            $user_id = Auth::user()->id;
            $orders = Order::where('user_id', $user_id)->get();
        }
        return view('order.list', compact('orders'));
    }

    public function prepaid(){
        return view('order.prepaid');
    }

    public function product(){
        return view('order.product');
    }

    public function order_success(){
        $order_id = request()->order_id;
        $total = request()->total;
        $phone = request()->phone;
        $address = request()->address;
        $product = request()->product;
        $grandTotal5 = request()->total * 0.05;
        return view('order.success', compact('order_id', 'total', 'phone', 'grandTotal5', 'address', 'product'));
    }

    public function order_pay($order_id){
        return view('order.pay', compact('order_id'));
    }

    public function prepaidStore(Request $request){
        
        //validation
        $this->validate($request,[
            'phone' => 'required',
        ]);
        $count=0;
        $random_number='';
        while ( $count < 10 ) {
            $random_digit = mt_rand(0, 9);
            
            $random_number .= $random_digit;
            $count++;
        }
        $grandTotal = $request->value * 0.05;
        // create new data
        $user_id = Auth::user()->id;
        $order = new Order;
        $order->user_id = $user_id;
        $order->order_number = $random_number;
        $order->order_type = "PrepaidOrder";
        $order->status = "pending";
        $order->phone = $request->phone;
        $order->total = $request->value;
        $order->grand_total = $grandTotal + $request->value;
        $order->save();
        return redirect()->route('order.success', ['order_id' => $random_number, 'total' => $request->value, 'phone' => $request->phone]);
    }

    public function productStore(Request $request){
        
        //validation
        $this->validate($request,[
            'product' => 'required',
            'address' => 'required',
            'price' => 'required',
        ]);
        $count=0;
        $random_number='';
        while ( $count < 10 ) {
            $random_digit = mt_rand(0, 9);
            
            $random_number .= $random_digit;
            $count++;
        }
        // create new data
        $user_id = Auth::user()->id;
        $order = new Order;
        $order->user_id = $user_id;
        $order->order_number = $random_number;
        $order->order_type = "ProductOrder";
        $order->status = "pending";
        $order->product = $request->product;
        $order->address = $request->address;
        $order->total = $request->price;
        $order->grand_total = 10000 + $request->price;
        $order->save();
        return redirect()->route('order.success', ['order_id' => $random_number, 'total' => $request->price, 'product' => $request->product, 'address' => $request->address]);
    }

    public function orderStore(Request $request){
        
        //validation
        $this->validate($request,[
            'order_number' => 'required',
        ]);
        $order = Order::where('order_number', $request->order_number)->first();
        if ($order == null || $order->user_id != Auth::user()->id) {
            return redirect()->route('order.list')->with('alert-success', 'Order tidak ditemukan');
        } else {
            if($order->order_type == "ProductOrder") {
                $strRand = "";
                $characters = array_merge(range('A','Z'));
                $max = count($characters) - 1;
                for ($i = 0; $i < 8; $i++) {
                    $rand = mt_rand(0, $max);
                    $strRand .= $characters[$rand];
                }
                $order->shipping_code = $strRand;
            }
            $order->status = "success";
            $order->save();
        }
        return redirect()->route('order.list');
    }
}
