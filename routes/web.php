<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/prepaid-balance', 'OrderController@prepaid')->name('prepaid-balance')->middleware('auth');
Route::get('/products', 'OrderController@product')->name('products')->middleware('auth');
Route::get('/order-success', 'OrderController@order_success')->name('order.success');
Route::get('/order-pay/{order_id}', 'OrderController@order_pay')->name('order.pay');
Route::get('/order', 'OrderController@index')->name('order.list')->middleware('auth');

// prepaid Store
Route::post('/order-store', [
    'uses' => 'OrderController@orderStore',
    'as' => 'order.store'
]);

// prepaid Store
Route::post('/order-prepaid', [
    'uses' => 'OrderController@prepaidStore',
    'as' => 'prepaid.store'
]);

// product Store
Route::post('/order-product', [
    'uses' => 'OrderController@productStore',
    'as' => 'product.store'
]);
